import React, { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import { fetchData } from '../../services/fetchData';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import TextField from '@material-ui/core/TextField';


const useStyles = makeStyles(theme => ({
    root: {
        width: '100%',
        overflowX: 'auto',
        paddingTop: '5px'
    },
    table: {
        minWidth: 650,
    },
    tableWrapper: {
        maxHeight: 'calc(100vh - 150px)',
        overflow: 'auto',
    },
    formControl: {
        margin: theme.spacing(1),
        minWidth: 120,
    },
    selectEmpty: {
        marginTop: theme.spacing(2),
    }
}));

export const List = () => {
    const classes = useStyles();
    const [rows, setRows] = useState([]);
    const [orignalRows, setOrignalRows] = useState([]);
    const inputLabel = React.useRef(null);
    const [values, setValues] = React.useState({
        by: 'popular',
        type: 'movie',
    });
    const [labelWidth, setLabelWidth] = React.useState(0);
    useEffect(() => {
        setLabelWidth(inputLabel.current.offsetWidth);
        fetchData({
            by: 'popular',
            type: 'movie'
        }).then(data => {
            setRows(data.results);
            setOrignalRows(data.results);
        });
    }, []);
    const handleChange = event => {
        setValues(oldValues => {
            const newValue = {
                ...oldValues,
                [event.target.name]: event.target.value,
            }
            fetchData(newValue).then(data => {
                setRows(data.results);
                setOrignalRows(data.results);
            });
            return newValue;
        });
    };

    const handleSearch = event => {
        const newRows = orignalRows.filter(row => {
            const value = event.target.value.toLowerCase();
            return (row.title && row.title.toLowerCase().indexOf(value) > -1) ||
                   (row.name && row.name.toLowerCase().indexOf(value) > -1);
        });
        setRows(newRows);
    }

    return (
        <Paper className={classes.root}>
            <div className={classes.top}>
                <i>IMDB List </i>
            </div>
            <FormControl variant="outlined" className={classes.formControl}>
                <InputLabel ref={inputLabel} htmlFor="outlined-type-simple">
                    Type
                </InputLabel>
                <Select
                    value={values.type}
                    onChange={handleChange}
                    labelWidth={labelWidth}
                    inputProps={{
                        name: 'type',
                        id: 'outlined-type-simple',
                    }}
                >
                    <MenuItem value={'movie'}>Movie</MenuItem>
                    <MenuItem value={'tv'}>TV</MenuItem>
                </Select>
            </FormControl>
            <FormControl variant="outlined" className={classes.formControl}>
                <InputLabel ref={inputLabel} htmlFor="outlined-by-simple">
                    By
                </InputLabel>
                <Select
                    value={values.by}
                    onChange={handleChange}
                    labelWidth={labelWidth}
                    inputProps={{
                        name: 'by',
                        id: 'outlined-by-simple',
                    }}
                >
                    <MenuItem value={'popular'}>Popular</MenuItem>
                    <MenuItem value={'trending'}>Trending</MenuItem>
                    <MenuItem value={'now_playing'}>Newest</MenuItem>
                    <MenuItem value={'top_rated'}>Highest</MenuItem>
                </Select>
            </FormControl>
            <TextField
                id="standard-search"
                label="Search"
                type="search"
                onChange={handleSearch}
                className={classes.textField}
                margin="normal"
            />
            <div className={classes.tableWrapper}>
                <Table className={classes.table} stickyHeader>
                    <TableHead>
                        <TableRow>
                            <TableCell>Title</TableCell>
                            <TableCell align="right">Media</TableCell>
                            <TableCell align="right">Genre</TableCell>
                            <TableCell align="right">Release Date</TableCell>
                            <TableCell align="right">Rating</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {rows.map(row => (
                            <TableRow key={row.id}>
                                <TableCell component="th" scope="row">
                                    {row.title || row.name}
                                </TableCell>
                                <TableCell align="right">{row.media_type}</TableCell>
                                <TableCell align="right">{row.genresString}</TableCell>
                                <TableCell align="right">{row.release_date || row.first_air_date}</TableCell>
                                <TableCell align="right">{row.vote_average}</TableCell>
                            </TableRow>
                        ))}
                    </TableBody>
                </Table>
            </div>
        </Paper>
    );
}