const key = "3a94078fb34b772a31d9a1348035bed7"


export const fetchData = async (values) => {
    let url = '';
    if (values.by === "trending") {
        url = `https://api.themoviedb.org/3/trending/${values.type}/day?api_key=${key}`;
    } else if (values.type === 'tv' && values.by === "now_playing") {
        url = `https://api.themoviedb.org/3/${values.type}/on_the_air?api_key=${key}`
    }
    else {
        url = `https://api.themoviedb.org/3/${values.type}/${values.by}?api_key=${key}`
    }
    const data = await fetch(url);
    const jsonData = await data.json();
    let genres = [];
    if (values.type === 'tv') {
        const data = await fetch(`https://api.themoviedb.org/3/genre/tv/list?api_key=${key}&language=en-US`)
        const genresJson = await data.json();
        genres = genresJson.genres;
    } else {
        const data = await fetch(`https://api.themoviedb.org/3/genre/movie/list?api_key=${key}&language=en-US`)
        const genresJson = await data.json();
        genres = genresJson.genres;
    }

    jsonData.results.forEach(element => {
        let genresString = '';
        element.media_type = values.type;
        element.genre_ids.forEach(id => {
            const genre = genres.find(genre => id === genre.id);
            genresString += genre ? genre.name + ", " : "";
        });
        element.genresString = genresString.length > 0 ? genresString.substring(0, genresString.length - 2) : genresString;
    });
    return jsonData;
}